var router = require('express').Router();
var authentication = require('express-authentication');
var rh = require('./../helpers/resHelper');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');


var bundleService = require('./../services/bundleService');

var routerPrefix = '/';
router.post(routerPrefix , [
    check('description').exists().isLength({ max: 1024 }),
    check('price').exists().isNumeric(),
    check('cost').exists().isNumeric(),
    check('UserId').exists(),
    check('WithId').exists(),
    check('ToId').exists(),
    check('status').exists(),
    authentication.required()
  ], async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res, await bundleService.add(req.body));
    }catch(e){
        rh.Error(req,res,e);
    }
})

router.get(routerPrefix, authentication.required(), async function(req, res) {
    res.json(await bundleService.get());
})

router.get(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await bundleService.getById(req.params.id));
})

router.put(routerPrefix + ":id", authentication.required(), async function(req, res) {
  res.json(await bundleService.edit(req.params.id, req.body));
})

router.delete(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await bundleService.delete(req.params.id));
})


module.exports = router