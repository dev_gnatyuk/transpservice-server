var express = require('express');
var rh = require('./../helpers/resHelper');
var authentication = require('express-authentication');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

router = express.Router();
/*Services*/
var UserService = require('./../services/userService');
var RoleService = require('./../services/roleService');

var routerPrefix = "/";

router.get(routerPrefix, authentication.required(), async function(req, res) {
  try{
    rh.Ok(req,res, await UserService.get());
  }catch(e){
    rh.Error(req,res,e);
  }
})

router.get(routerPrefix + ':id', authentication.required(),async function(req, res) {
  try{
    rh.Ok(req,res, await UserService.getById(req.params.id));
  }catch(e){
    rh.Error(req,res,e);
  }
})

router.post(routerPrefix, 
  [
    check('name').exists().isLength({ max: 256 }),
    check('email').exists().isEmail().trim().normalizeEmail(),
    check('role').exists().isIn(RoleService.get()),
    check('password').exists().isLength({ max: 64 }),
    authentication.required()
  ],async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res, await UserService.registration(req.body));
    }catch(e){
        rh.Error(req,res,e);
    }
})

router.put(routerPrefix + ':id', [
    check('name').exists().isLength({ max: 256 }),
    check('email').exists().isEmail().trim().normalizeEmail(),
    check('role').exists().isIn(RoleService.get()),
    check('password').isLength({ max: 64 }),
    authentication.required()
  ], async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res, await UserService.edit(req.params.id, req.body));
    }catch(e){
        rh.Error(req,res,e);
    }
})

router.get(routerPrefix, authentication.required(), async function(req, res) {
  res.json(await UserService.get());
})

module.exports = router