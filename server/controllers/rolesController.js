var router = require('express').Router();
var authentication = require('express-authentication');
var rh = require('./../helpers/resHelper');

var routerPrefix = "/";

var RoleService = require('./../services/roleService');


router.get(routerPrefix, authentication.required(), function(req, res) {
    res.json(RoleService.get());
})

module.exports = router