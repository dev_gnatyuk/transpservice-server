var router = require('express').Router();
var authentication = require('express-authentication');
var rh = require('./../helpers/resHelper');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

var routerPrefix = '/';

var transportingService = require('./../services/transportingService');


router.post(routerPrefix, [
    check('duration').exists().isNumeric(),
    check('FlightId').exists().isNumeric(),
    check('WithId').exists().isNumeric(),
    check('ToId').exists().isNumeric(), 
    authentication.required()
], async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res, await transportingService.add(req.body));
    }catch(e){
        rh.Error(req,res,e);
    }
})

router.get(routerPrefix + ":flightId", authentication.required(), async function(req, res) {
    res.json(await transportingService.get({FlightId: req.params.flightId}));
})

router.get(routerPrefix + ":flightId/" + ":id", authentication.required(), async function(req, res) {
    res.json(await transportingService.getById(req.params.flightId, req.params.id));
})

router.put(routerPrefix + ":flightId/" + ":id", [
    check('duration').exists().isNumeric(),
    check('FlightId').exists().isNumeric(),
    check('WithId').exists().isNumeric(),
    check('ToId').exists().isNumeric(), 
    authentication.required()
], async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res, await transportingService.edit(req.params.flightId, req.params.id, req.body));
    }catch(e){
        rh.Error(req,res,e);
    }

  //res.json(await transportingService.edit(req.params.id, req.body));
})

router.delete(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await transportingService.delete(req.params.id));
})


module.exports = router