var router = require('express').Router();

var path = require('path');

router.use('/security',require('./securityController'));
router.use('/cars', require('./carsController'));
router.use('/offices',require('./officesController'));
router.use('/users', require('./usersController'));
router.use('/bundles', require('./bundlesController'));
router.use('/flights', require('./flightsController'));
router.use('/transportings', require('./transportingsController'));
router.use('/roles', require('./rolesController'));

/*router.get('/', function(req, res) {
    //res.render('index')
    //res.sendFile(path.join(__dirname + '/../views/index.html'));
    //res.redirect('/');
})*/

module.exports = router