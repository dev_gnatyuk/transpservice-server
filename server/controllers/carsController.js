var router = require('express').Router();
var authentication = require('express-authentication');
var rh = require('./../helpers/resHelper');

var routerPrefix = '/';

var carService = require('./../services/carService');


router.post(routerPrefix, authentication.required(), async function(req, res) {
    res.json(await carService.add(req.body));
})

router.get(routerPrefix, authentication.required(), async function(req, res) {
    res.json(await carService.get());
})

router.get(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await carService.getById(req.params.id));
})

router.put(routerPrefix + ":id", authentication.required(), async function(req, res) {
  res.json(await carService.edit(req.params.id, req.body));
})

router.delete(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await carService.delete(req.params.id));
})


module.exports = router