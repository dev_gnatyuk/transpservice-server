var router = require('express').Router();
var authentication = require('express-authentication');
var rh = require('./../helpers/resHelper');
const { check, validationResult } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');

var routerPrefix = '/';

var flightService = require('./../services/flightService');


router.post(routerPrefix, [
    check('number').exists().isLength({ max: 256 }),
    check('command').exists(),
    check('CarId').exists(),
    authentication.required()
  ], async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res, await flightService.add(req.body));
    }catch(e){
        rh.Error(req,res,e);
    }
})

router.get(routerPrefix, async function(req, res) {
    res.json(await flightService.get());
})

router.get(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await flightService.getById(req.params.id));
})

router.put(routerPrefix + ":id", [
    check('number').exists().isLength({ max: 256 }),
    check('command').exists(),
    check('CarId').exists(),
    authentication.required()
  ], async function(req, res) {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.mapped() });
    }
    try{
        rh.Ok(req,res, await flightService.edit(req.params.id, req.body));
    }catch(e){
        rh.Error(req,res,e);
    }
})

router.delete(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await flightService.delete(req.params.id));
})


module.exports = router