var router = require('express').Router();
var authentication = require('express-authentication');
var rh = require('./../helpers/resHelper');

var routerPrefix = "/";

var officeService = require('./../services/officeService');


router.post(routerPrefix, authentication.required(), async function(req, res) {
    res.json(await officeService.add(req.body));
})

router.get(routerPrefix, authentication.required(), async function(req, res) {
    res.json(await officeService.get());
})

router.get(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await officeService.getById(req.params.id));
})

router.put(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await officeService.edit(req.params.id, req.body));
})

router.delete(routerPrefix + ":id", authentication.required(), async function(req, res) {
    res.json(await officeService.delete(req.params.id));
})

module.exports = router