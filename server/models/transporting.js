'use strict';
module.exports = (sequelize, DataTypes) => {
  var Transporting = sequelize.define('Transporting', {
    duration: DataTypes.DECIMAL
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Transporting;
};