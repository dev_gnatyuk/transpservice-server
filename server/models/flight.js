'use strict';
module.exports = (sequelize, DataTypes) => {
  var Flight = sequelize.define('Flight', {
    number: DataTypes.STRING,
    command: DataTypes.STRING,
    startDate: DataTypes.DATE
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Flight;
};