'use strict';
module.exports = (sequelize, DataTypes) => {
  var Bundle = sequelize.define('Bundle', {
    description: DataTypes.STRING,
    price: DataTypes.DECIMAL,
    cost: DataTypes.DECIMAL,
    status: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Bundle;
};