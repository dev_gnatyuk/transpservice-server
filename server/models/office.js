'use strict';
module.exports = (sequelize, DataTypes) => {
  var Office = sequelize.define('Office', {
    name: DataTypes.STRING,
    location: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Office;
};