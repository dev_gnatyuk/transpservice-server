"use strict";

var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");
var env       = process.env.NODE_ENV || "development";
var config    = require(path.join(__dirname, '..', 'config', 'config.json'))[env];
if (process.env.DATABASE_URL) {
  var sequelize = new Sequelize(process.env.DATABASE_URL,config);
} else {
  var sequelize = new Sequelize(config.database, config.username, config.password, config);
}
var db        = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.Transporting.hasMany(db.TransportingBundle);

db.TransportingBundle.belongsTo(db.Transporting);
db.TransportingBundle.belongsTo(db.Bundle);

db.Bundle.belongsTo(db.User);

db.Flight.belongsTo(db.Car);

db.Transporting.belongsTo(db.Flight);

db.Bundle.belongsTo(db.Office, { as: 'With' });
db.Bundle.belongsTo(db.Office, { as: 'To' });
db.Bundle.belongsTo(db.Office, { as: 'Current' });

db.Transporting.belongsTo(db.Office, { as: 'With' });
db.Transporting.belongsTo(db.Office, { as: 'To' });
module.exports = db;