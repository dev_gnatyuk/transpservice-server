var models  = require('./../models');
var Car  = require('./../models/car');

var carService = {

    add: async function(data) {
        var car = {
            name: data.name,
            number: data.number
        };
        return await models.Car.create(car);
    },

    edit: async function(id, data) {
        var car = await this.getById(id);
        if (car) {
            car.name = data.name;
            car.number = data.number;
        }
        return await car.save(car);
    },

    getById: async function(id) {
        return await models.Car.findOne({ where: {
            id: id
        }});
    },

    delete: async function (id){
        return await models.Car.destroy({where: {
            id: id
        }});
    },

    get: async function(where = {}){
        return await models.Car.findAll({ where: where });
    }
};

module.exports = carService;