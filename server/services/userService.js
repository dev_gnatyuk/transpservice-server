var models  = require('./../models');
var User  = require('./../models/user');
var CryptoService = require('./cryptoService');
var RoleService = require('./roleService');

var userService = {
        login: async function(email, password) {
            if (email && password){
                var user = await models.User.findOne({ where: {
                    email: email, 
                    password: CryptoService.MD5(password)
                }});
            }
            
            if (!user){
                throw new Error("Email or password incorrected");
            }else{
                user.token = CryptoService.MD5(Date.now() + " - " + user.id);
            }
            await user.save(user);
            return {auth_token: user.token};
        },
    
        registration: async function(data) {

            var user = {
                name: data.name,
                email: data.email,
                role: data.role,
                password: CryptoService.MD5(data.password)
            };
            return await models.User.create(user);
        },
    
        getByToken: async function(token){
            return await models.User.findOne({ where: {
                token: token
            }});
        },

        getById: async function(id) {
            return await models.User.findOne({ where: {
                id: id
            }});
        },
    
        delete: async function (id){
            return await models.User.destroy({where: {
                id: id
            }});
        },
    
        get: async function(where = {}){
            return await models.User.findAll({ where: where });
        },

        edit: async function(id, user){
            var oldUser = await this.getById(id);
            if (!oldUser){
                throw new Error("User not found");
            }
            oldUser.name = user.name;
            oldUser.email = user.email;
            oldUser.role = user.role;
            if (user.password.length > 0){
                oldUser.password = CryptoService.MD5(user.password);
            }
            
            return await oldUser.save(oldUser);
        }
    };
    
    
    
    module.exports = userService;