var models  = require('./../models');
var Transporting  = require('./../models/transporting');

var bundleService = {

    add: async function(data) {
        var bundle = {
            description: data.description,
            price: data.price,
            cost: data.cost,
            status: data.status,
            UserId: data.UserId,
            WidtId: data.WidtId,
            ToId: data.ToId
        };
        return await models.Bundle.create(bundle);
    },

    edit: async function(id, data) {
        var bundle = await this.getById(id);
        if (bundle) {
            bundle.description = data.description;
            bundle.price = data.price;
            bundle.cost = data.cost;
            bundle.status = data.status;
            bundle.UserId = data.UserId;
            bundle.WidtId = data.WidtId;
            bundle.ToId = data.ToId;
            bundle.CurrentId = data.CurrentId;
        }
        return await bundle.save(bundle);
    },

    getById: async function(id, include = [ models.User, { model: models.Office, as: 'With' }, { model: models.Office, as: 'To' }]) {
        return await models.Bundle.findOne({include: include, where: {
            id: id
        }});
    },

    delete: async function (id){
        return await models.Bundle.destroy({where: {
            id: id
        }});
    },

    get: async function(where = {}, include = [ models.User, { model: models.Office, as: 'With' }, { model: models.Office, as: 'To' }] ){
        return await models.Bundle.findAll({include: include, where: where });
    }
};

module.exports = bundleService;