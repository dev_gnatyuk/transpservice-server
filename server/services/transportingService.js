var models  = require('./../models');
var Transporting  = require('./../models/transporting');

var transportingService = {

    add: async function(flightId, data) {
        var transporting = {
            WithId: data.WithId,
            ToId: data.ToId,
            FlightId: flightId,
            duration: data.duration
        };
        return await models.Transporting.create(transporting);
    },

    edit: async function(flightId, id, data) {
        var transporting = await this.getById(flightId, id);
        if (transporting) {
            transporting.WithId = data.WithId;
            transporting.ToId = data.ToId;
            transporting.FlightId = flightId;
            transporting.duration = data.duration;
            //transporting.relations = data.relations;
        }
        var tb = await transporting.getTransportingBundles();
        var r = await transporting.createTransportingBundle({TransportingId: id, BundleId: 1});
        await transporting.save(transporting);
        return await this.getById(flightId, id);
    },

    getById: async function(flightId, id) {
        return await models.Transporting.findOne({ include: [models.TransportingBundle], where: {
            id: id,
            FlightId: flightId
        }});
    },

    delete: async function (flightId, id){
        return await models.Transporting.destroy({where: {
            id: id,
            FlightId: flightId
        }});
    },

    get: async function(where = {}, include = [ models.Flight, { model: models.Office, as: 'With' }, { model: models.Office, as: 'To' }]){
        return await models.Transporting.findAll({include: include, where: where });
    }
};

module.exports = transportingService;