var models  = require('./../models');
var Transporting  = require('./../models/transporting');

var transportingBundleService = {

    add: async function(data) {
        var transportingBundle = {
            TransportingId: data.TransportingId,
            BundleId: data.BundleId
        };
        return await models.TransportingBundle.create(transporting);
    },

    edit: async function(id, data) {
        var transportingBundle = await this.getById(id);
        if (transportingBundle) {
            transportingBundle.TransportingId = data.TransportingId;
            transportingBundle.BundleId = data.BundleId;
        }
        return await models.TransportingBundle.save(transportingBundle);
    },

    getByTransportingId: async function(id) {
        return await models.TransportingBundle.findOne({ where: {
            TransportingId: id
        }});
    },

    delete: async function (id){
        return await models.TransportingBundle.destroy({where: {
            id: id
        }});
    },

    get: async function(where = {}){
        return await models.TransportingBundle.findAll({ where: where });
    }
};

module.exports = transportingBundleService;