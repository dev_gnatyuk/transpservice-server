var models  = require('./../models');
var Office  = require('./../models/office');

var officeService = {

    add: async function(data) {
        var office = {
            name: data.name,
            location: data.location
        };
        return await models.Office.create(office);
    },

    edit: async function(id, data) {
        var office = await this.getById(id);
        if (office) {
            office.name = data.name;
            office.location = data.location;
        }
        return await models.Office.save(office);
    },

    getById: async function(id) {
        return await models.Office.findOne({ where: {
            id: id
        }});
    },

    delete: async function (id){
        return await models.Office.destroy({where: {
            id: id
        }});
    },

    get: async function(where = {}){
        return await models.Office.findAll({ where: where });
    }
};

module.exports = officeService;