var models  = require('./../models');
var Office  = require('./../models/office');

var roleService = {

    get: () => {
        return [
            'Admin', 'Driver', 'Client'
        ];
    },

    check: (name) =>{
        return roleService.get().find(x=>x === name) != null;
    }
};

module.exports = roleService;