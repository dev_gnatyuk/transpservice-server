
var UserService = require('./userService');

var SecurityService = {

    getByToken: async function(token){
        return await UserService.getByToken(token);
    }

}

module.exports = SecurityService;