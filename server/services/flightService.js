var models  = require('./../models');
var Flight  = require('./../models/flight');

var flightService = {

    add: async function(data) {
        var flight = {
            CarId: data.CarId,
            number: data.number,
            command: data.command
        };
        return await models.Flight.create(flight);
    },

    edit: async function(id, data) {
        var flight = await this.getById(id);
        if (flight) {
            flight.CarId = data.CarId;
            flight.number = data.number;
            flight.command = data.command;
        }
        return await flight.save(flight);
    },

    getById: async function(id) {
        return await models.Flight.findOne({ where: {
            id: id
        }});
    },

    delete: async function (id){
        return await models.Flight.destroy({where: {
            id: id
        }});
    },

    get: async function(where = {}){
        return await models.Flight.findAll({ where: where });
    }
};

module.exports = flightService;