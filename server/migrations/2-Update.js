'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * removeColumn "date" from table "Flights"
 * addColumn "startDate" to table "Flights"
 *
 **/

var info = {
    "revision": 2,
    "name": "Update",
    "created": "2017-11-19T14:47:06.148Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "removeColumn",
        params: ["Flights", "date"]
    },
    {
        fn: "addColumn",
        params: [
            "Flights",
            "startDate",
            {
                "type": Sequelize.DATE
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
