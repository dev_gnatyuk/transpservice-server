var cors = require('cors')
var aa = require('express-async-await');
var express     = aa(require('express'));
require('express-async-errors');
var authentication = require('express-authentication');
var app         = express();
app.use(cors());
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var securityService = require('./server/services/securityService');
//var mongoose    = require('mongoose'); 

var jwt    = require('jsonwebtoken');
var config = require('./config');

//var User = require('./server/models/user'); // get our mongoose model

var port = process.env.PORT || 8080;

/*mongoose.connect(config.database,{
    useMongoClient: true
  });*/
app.set('superSecret', config.secret);

//app.set('views', __dirname + '/views')
//app.engine('jade', require('jade').__express)
//app.set('view engine', 'jade')
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(async function myauth(req, res, next) {
    // provide the data that was used to authenticate the request; if this is 
    // not set then no attempt to authenticate is registered. 
    //req.challenge = req.get('Authorization');
    var token = req.get('Authorization');
    var user = await securityService.getByToken(token);
    req.authenticated = user ? true : false;
    // provide the result of the authentication; generally some kind of user 
    // object on success and some kind of error as to why authentication failed 
    // otherwise. 
    if (req.authenticated) {
        req.authentication = user.dataValues;
    } else {
        req.authentication = { error: 'INVALID_API_KEY' };
    }
 
    // That's it! You're done! 
    next();
});

app.use('/api', require('./server/controllers/index'));

app.listen(port, function() {
    console.log('Listening on port ' + port)
});

app.use((err, req, res, next) => {
    res.status(err.status == null ? 500 : err.status).json({error: err.error});
});